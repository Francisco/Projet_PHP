<?php
  class Commentaire{

    public function addCommentaire($materiel, $utilisateur, $description){
      $connexion=connect();
      $sql="INSERT INTO COMMENTAIRE(MATERIEL, UTILISATEUR, DESCRIPTION)
        VALUES(:materiel, :utilisateur, :description)";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':materiel',$materiel, PDO::PARAM_INT);
      $stmt->bindParam(':utilisateur',$utilisateur, PDO::PARAM_INT);
      $stmt->bindParam(':description',$description, PDO::PARAM_STR);
      $stmt->execute();
      return array('materiel'=>$materiel,'utilisateur'=>$utilisateur, 'description'=>$description);
    }

    public function suppCommentaire($id){
      $connexion=connect_db();
      $sql="DELETE FROM COMMENTAIRE WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_STR);
      $stmt->execute();
    }

    public function editCommentaire($id, $materiel, $utilisateur, $description){
      $connexion=connect();
      $sql="UPDATE COMMENTAIRE SET MATERIEL=:materiel, UTILISATEUR=:utilisateur,
        DESCRIPTION=:description where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->bindParam(':materiel',$materiel, PDO::PARAM_INT);
      $stmt->bindParam(':utilisateur',$utilisateur, PDO::PARAM_INT);
      $stmt->bindParam(':description',$description, PDO::PARAM_STR);
      $stmt->execute();
      return array('materiel'=>$materiel,'utilisateur'=>$utilisateur, 'description'=>$description);
    }

    public function getCommentaire($id){
      $connexion=connect();
      $sql="SELECT * from COMMENTAIRE where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getMateriel($id){
      $connexion=connect();
      $sql="SELECT mat.NOM, mat.DESCRIPTION, mat.POIDS, mat.HAUTEUR, mat.PRIX, mat.LARGEUR
        from COMMENTAIRE com LEFT JOIN MATERIEL mat ON com.ID=mat.ID WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getUtilisateur($id){
      $connexion=connect();
      $sql="SELECT util.NOM, util.PRENOM, util.MAIL, util.TELEPHONE
        from COMMENTAIRE com LEFT JOIN UTILISATEUR util ON com.ID=util.ID WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getDescription($id){
      $connexion=connect();
      $sql="SELECT DESCRIPTION from COMMENTAIRE where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

  }
?>
