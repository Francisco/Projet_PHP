<?php
  class Utilisateur{

    public function addUtilisateur(){
      $connexion=connect();
      $sql="INSERT INTO UTILISATEUR(NOM, PRENOM, MAIL, TELEPHONE) SET VALUES(:nom, :prenom, :mail, :telephone)";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':nom',$nom, PDO::PARAM_STR);
      $stmt->bindParam(':prenom',$description, PDO::PARAM_STR);
      $stmt->bindParam(':mail',$largeur, PDO::PARAM_STR);
      $stmt->bindParam(':telephone',$hauteur, PDO::PARAM_STR);
      $stmt->execute();
      return array('nom'=>$nom,'prenom'=>$prenom,'mail'=>$mail,'telephone'=>$telephone);
    }

    public function suppUtilisateur($id){
      $connexion=connect_db();
      $sql="DELETE FROM UTILISATEUR WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_STR);
      $stmt->execute();
    }

    public function editUtilisateur($id){
      $connexion=connect();
      $sql="UPDATE TABLE UTILISATEUR SET NOM=:nom,PRENOM=:prenom,MAIL=:mail,TELEPHONE=:telephone
        WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->bindParam(':nom',$nom, PDO::PARAM_STR);
      $stmt->bindParam(':prenom',$description, PDO::PARAM_STR);
      $stmt->bindParam(':mail',$largeur, PDO::PARAM_STR);
      $stmt->bindParam(':telephone',$hauteur, PDO::PARAM_STR);
      $stmt->execute();
      return array('nom'=>$nom,'prenom'=>$prenom,'mail'=>$mail,'telephone'=>$telephone);
    }

    public function getUtilisateur($id){
      $connexion=connect();
      $sql="SELECT * from UTILISATEUR where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getNom($id){
      $connexion=connect();
      $sql="SELECT NOM from UTILISATEUR where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getPrenom($id){
      $connexion=connect();
      $sql="SELECT PRENOM from UTILISATEUR where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getMail($id){
      $connexion=connect();
      $sql="SELECT MAIL from UTILISATEUR where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getTelephone($id){
      $connexion=connect();
      $sql="SELECT TELEPHONE from UTILISATEUR where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }
  }
?>
