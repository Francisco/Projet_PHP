<?php
  class Conseil{

    public function addConseil($utilisateur, $description){
      $connexion=connect();
      $sql="INSERT INTO COMMENTAIRE(UTILISATEUR, DESCRIPTION) VALUES(:utilisateur, :description)";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':utilisateur',$utilisateur, PDO::PARAM_INT);
      $stmt->bindParam(':description',$description, PDO::PARAM_STR);
      $stmt->execute();
      return array('utilisateur'=>$utilisateur, 'description'=>$description);
    }

    public function suppConseil($id){
      $connexion=connect_db();
      $sql="DELETE FROM CONSEIL WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_STR);
      $stmt->execute();
    }

    public function editConseil($id, $utilisateur, $description){
      $connexion=connect();
      $sql="UPDATE CONSEIL SET UTILISATEUR=:utilisateur, DESCRIPTION=:description where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->bindParam(':utilisateur',$utilisateur, PDO::PARAM_INT);
      $stmt->bindParam(':description',$description, PDO::PARAM_STR);
      $stmt->execute();
      return array('utilisateur'=>$utilisateur, 'description'=>$description);
    }

    public function getConseil($id){
      $connexion=connect();
      $sql="SELECT * from CONSEIL where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getUtilisateur($id){
      $connexion=connect();
      $sql="SELECT util.NOM, util.PRENOM, util.MAIL, util.TELEPHONE
        from CONSEIL cons LEFT JOIN UTILISATEUR util ON cons.ID=util.ID WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getDescription($id){
      $connexion=connect();
      $sql="SELECT DESCRIPTION from COMMENTAIRE where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

  }
?>
