<?php
use connection;
    class Materiel{

      public function addMateriel($nom,$description,$largeur,$hauteur,$poids,$prix){
        $connexion=connect();
        $sql="INSERT INTO MATERIEL(NOM,DESCRIPTION,LARGEUR,HAUTEUR,POIDS,PRIX) VALUES (:nom,:description,:largeur,:hauteur,:poids,:prix)";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':nom',$nom, PDO::PARAM_STR);
        $stmt->bindParam(':description',$description, PDO::PARAM_STR);
        $stmt->bindParam(':largeur',$largeur, PDO::PARAM_INT);
        $stmt->bindParam(':hauteur',$hauteur, PDO::PARAM_INT);
        $stmt->bindParam(':poids',$poids, PDO::PARAM_INT);
        $stmt->bindParam(':prix',$prix, PDO::PARAM_INT);
        $stmt->execute();
        return array('nom'=>$nom,'description'=>$description,'taille'=>$taille,'poids'=>$poids, 'prix'=>$prix);

      }

      public function suppMateriel($id){
        $connexion=connect_db();
        $sql="DELETE FROM MATERIEL WHERE ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        }

      public function editMateriel($id,$nom,$description,$taille,$poids,$prix){
        $connexion=connect();
        $sql="UPDATE TABLE MATERIEL SET NOM=:nom,DESCRIPTION=:description,LARGEUR=:largeur,HAUTEUR=:hauteur,
          POIDS=:poids,PRIX=:prix WHERE ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->bindParam(':nom',$nom, PDO::PARAM_STR);
        $stmt->bindParam(':description',$description, PDO::PARAM_STR);
        $stmt->bindParam(':largeur',$largeur, PDO::PARAM_INT);
        $stmt->bindParam(':hauteur',$hauteur, PDO::PARAM_INT);
        $stmt->bindParam(':poids',$poids, PDO::PARAM_INT);
        $stmt->bindParam(':prix',$prix, PDO::PARAM_INT);
        $stmt->execute();
        return array('nom'=>$nom,'description'=>$description,'taille'=>$taille,'poids'=>$poids, 'prix'=>$prix);
      }

      public function getMateriel($id){
        $connexion=connect();
        $sql="SELECT * from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }

      public function getNom($id){
        $connexion=connect();
        $sql="SELECT NOM from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }

      public function getHauteur($id){
        $connexion=connect();
        $sql="SELECT HAUTEUR from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }
      public function getLargeur($id){
        $connexion=connect();
        $sql="SELECT LARGEUR from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }

      public function getPoids($id){
        $connexion=connect();
        $sql="SELECT POIDS from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }

      public function getPrix($id){
        $connexion=connect();
        $sql="SELECT PRIX from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }

      public function getDescription($id){
        $connexion=connect();
        $sql="SELECT DESCRIPTION from MATERIEL where ID=:id";
        $stmt=$connexion->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
      }

    }
?>
