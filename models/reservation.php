<?php
  class Reservation{

    public function addReservation($materiel, $utilisateur, $reservationDate, $description){
      $connexion=connect();
      $sql="INSERT INTO RESERVATION(MATERIEL, UTILISATEUR, RESERVATIONDATE, DESCRIPTION)
        VALUES(:materiel, :utilisateur, :reservationDate, :description)";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':materiel',$id, PDO::PARAM_INT);
      $stmt->bindParam(':utilisateur',$materiel, PDO::PARAM_INT);
      $stmt->bindParam(':reservationDate',$reservationDate, PDO::PARAM_STR);
      $stmt->bindParam(':description',$description, PDO::PARAM_STR);
      $stmt->execute();
      return array('materiel'=>$materiel,'utilisateur'=>$utilisateur,'reservationDate'=>$reservationDate, 'description'=>$description);
    }

    public function suppReservation($id){
      $connexion=connect_db();
      $sql="DELETE FROM RESERVATION WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_STR);
      $stmt->execute();
    }

    public function editReservation($id, $materiel, $utilisateur, $reservationDate, $description){
      $connexion=connect();
      $sql="UPDATE RESERVATION SET MATERIEL=:materiel, UTILISATEUR=:utilisateur,
        RESERVATIONDATE=:reservationDate, DESCRIPTION=:description where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->bindParam(':materiel',$materiel, PDO::PARAM_INT);
      $stmt->bindParam(':utilisateur',$materiel, PDO::PARAM_INT);
      $stmt->bindParam(':reservationDate',$reservationDate, PDO::PARAM_STR);
      $stmt->bindParam(':description',$description, PDO::PARAM_STR);
      $stmt->execute();
      return array('materiel'=>$materiel,'utilisateur'=>$utilisateur,'reservationDate'=>$reservationDate, 'description'=>$description);
    }

    public function getReservation($id){
      $connexion=connect();
      $sql="SELECT * from RESERVATION where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getMateriel($id){
      $connexion=connect();
      $sql="SELECT mat.NOM, mat.DESCRIPTION, mat.POIDS, mat.HAUTEUR, mat.PRIX, mat.LARGEUR
        from RESERVATION res LEFT JOIN MATERIEL mat ON res.ID=mat.ID WHERE ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getUtilisateur($id){
      $connexion=connect();
      $sql="SELECT util.NOM, util.PRENOM, util.MAIL, util.TELEPHONE
        from RESERVATION res LEFT JOIN UTILISATEUR util ON res.ID=util.ID WHEREwhere ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getDate($id){
      $connexion=connect();
      $sql="SELECT RESERVATIONDATE from RESERVATION where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }

    public function getDescription($id){
      $connexion=connect();
      $sql="SELECT DESCRIPTION from RESERVATION where ID=:id";
      $stmt=$connexion->prepare($sql);
      $stmt->bindParam(':id',$id, PDO::PARAM_INT);
      $stmt->execute();
      return $stmt->fetch();
    }
  }
?>
